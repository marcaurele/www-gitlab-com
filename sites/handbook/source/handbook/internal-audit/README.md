# The Internal Audit Function has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/internal-audit](https://handbook.gitlab.com/handbook/internal-audit)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/internal-audit](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/internal-audit)

If you need help or assitance with this please reach out to @jamiemaynard (Developer/Handbooks) or
@marshall007 (DRI Content Sites).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)

