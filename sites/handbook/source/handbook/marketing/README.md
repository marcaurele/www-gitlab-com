# marketing has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/marketing](https://handbook.gitlab.com/handbook/marketing)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/marketing](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/marketing)

If you need help or assistance with this please reach out to @jamiemaynard (Developer/Handbooks) or
@marshall007 (DRI Content Sites).  Alternatively ask your questions on slack in [#handbook](https://gitlab.slack.com/archives/C81PT2ALD)

