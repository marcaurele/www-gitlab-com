---
layout: markdown_page
title: "GitLab to Announce Third Quarter Fiscal 2024 Financial Results"
description: "GitLab to Announce Third Quarter Fiscal 2024 Financial Results"
twitter_image: "/images/opengraph/Press-Releases/2023-fy24-q3-financial-results-date.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "GitLab to Announce Third Quarter Fiscal 2024 Financial Results"
---

**SAN FRANCISCO**, Nov. 13, 2023 – **All Remote** - GitLab Inc., (NASDAQ: GTLB), the most comprehensive AI-powered DevSecOps Platform, today announced that it will report its financial results for the third quarter of fiscal year 2024, which ended October 31, 2023, after U.S. markets close on Monday, December 4, 2023.

GitLab will host a Zoom video conference and earnings webcast beginning at 4:30 p.m. EST / 1:30 p.m. PST on the same day to discuss the company’s financial results. Interested parties may register for the conference call [here](https://gitlab.zoom.us/webinar/register/WN_lvtHHkZkSuWcvPCUgF-Y6g) or at ir.gitlab.com.

An archived replay of the webcast and a transcript of the prepared remarks will be available on the GitLab Investor Relations’ website at ir.gitlab.com.

GitLab uses its Investor Relations website [ir.gitlab.com](https://ir.gitlab.com/) and its Twitter feed ([@gitlab](https://twitter.com/gitlab)), among other channels, as a means of disclosing material nonpublic information and for complying with its disclosure obligations under Regulation FD.

**About GitLab Inc.**

GitLab is the most comprehensive AI-powered DevSecOps platform for software innovation. GitLab enables organizations to increase developer productivity, improve operational efficiency, reduce security and compliance risk, and accelerate digital transformation. More than 30 million registered users and more than 50% of the Fortune 100 trust GitLab to ship better, more secure software faster.

**Media Contact:** 
Lisa Boughner 
press@gitlab.com
